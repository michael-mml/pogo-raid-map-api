--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Ubuntu 11.4-1.pgdg16.04+1)
-- Dumped by pg_dump version 11.4 (Ubuntu 11.4-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: shared_raid_boss; Type: TABLE; Schema: public; Owner: buoerfvbllgfds
--

CREATE TABLE public.shared_raid_boss (
    name character varying(30) NOT NULL,
    tier integer,
    hatch_time time without time zone NOT NULL,
    hatch_date date NOT NULL,
    longitude real NOT NULL,
    latitude real NOT NULL,
    user_status boolean NOT NULL
);


ALTER TABLE public.shared_raid_boss OWNER TO buoerfvbllgfds;

--
-- Data for Name: shared_raid_boss; Type: TABLE DATA; Schema: public; Owner: buoerfvbllgfds
--

COPY public.shared_raid_boss (name, tier, hatch_time, hatch_date, longitude, latitude, user_status) FROM stdin;
\.


--
-- Name: shared_raid_boss shared_raid_boss_pkey; Type: CONSTRAINT; Schema: public; Owner: buoerfvbllgfds
--

ALTER TABLE ONLY public.shared_raid_boss
    ADD CONSTRAINT shared_raid_boss_pkey PRIMARY KEY (name, hatch_time, hatch_date, longitude, latitude);


--
-- Name: shared_raid_boss shared_raid_boss_fk; Type: FK CONSTRAINT; Schema: public; Owner: buoerfvbllgfds
--

ALTER TABLE ONLY public.shared_raid_boss
    ADD CONSTRAINT shared_raid_boss_fk FOREIGN KEY (name, tier) REFERENCES public.raid_boss(name, tier) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: shared_raid_boss shared_raid_boss_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: buoerfvbllgfds
--

ALTER TABLE ONLY public.shared_raid_boss
    ADD CONSTRAINT shared_raid_boss_name_fkey FOREIGN KEY (name) REFERENCES public.raid_boss(name);


--
-- Name: LANGUAGE plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO buoerfvbllgfds;


--
-- PostgreSQL database dump complete
--

