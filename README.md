# PoGo Raid Map API

## Getting Started

To run on a development machine:

1. Set `PUBLIC_IP` in your shell to your public IP address e.g. `export PUBLIC_IP=xxx.xx.xx.xxx`.

2. Run: `go run *.go`.

### PostgreSQL

To connect to the database: `psql -U $DB_USER -h $DB_HOST -d $DB_NAME`.

#### Installation

- run [`sudo apt install postgresql-common` then
`sudo sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh`](https://wiki.postgresql.org/wiki/Apt#Quickstart)
  - you may have to change the `METHOD` column in `pg_hba.conf` from `md5` to `trust` (for each row) if the default password doesn't work
    - if so, you need to set a password after logging in; run `ALTER USER postgres WITH PASSWORD 'YOUR_PASSWORD_HERE'`;
    - once set, revert the change (i.e. set `METHOD` back to `md5`)

- to resolve `there is no unique constraint matching given keys for referenced table` when attempting to reference a `FOREIGN KEY` in other tables:
    1. Ensure the referenced table has a `UNIQUE` constraint on the keys being referenced i.e.

        __`TABLE X`__

        ```sql
        # this creates a constraint that X.name and X.tier cannot be duplicated in another row
        # X.name and X.tier are being referenced by table Y
        "your_unique_constraint" UNIQUE CONSTRAINT, btree (name, tier)

        # The below also works if modifying an existing table
        # ALTER TABLE X ADD CONSTRAINT UNIQUE (name, tier)
        ```

        __`TABLE Y`__

        ```sql
        # to reference foreign keys (i.e. X.name and X.tier), they must have a UNIQUE constraint
        "your_constraint_fk" FOREIGN KEY (name, tier) REFERENCES X (name, tier) ON UPDATE CASCADE ON DELETE RESTRICT

        # The below also works if modifying an existing table
        # ALTER TABLE Y ADD CONSTRAINT your_constraint_fk FOREIGN KEY (name, tier) REFERENCES X (name, tier) ON UPDATE CASCADE ON DELETE RESTRICT;
        ```

- utilize `SERIAL` format for ids (DEPRECATED IN PostgreSQL 10?)
  - under CREATE SQL, uncomment `DROP TABLE public.YOUR_TABLE_NAME`, change the type of the id to `SERIAL` and execute
  - <https://www.postgresql.org/docs/current/datatype-numeric.html#DATATYPE-SERIAL>

#### Connection issues

- `pq: no pg_hba.conf entry for host`
  - set SSL to `require` (<https://devcenter.heroku.com/articles/heroku-postgres-ssl-brownouts#go>)

#### DB backups

##### Backup

- use `pg_dump [DATABASE_CONNECTION_STRING_HERE] --table public.[YOUR_TABLE_HERE]`

##### Restore

For local development (with a PostgreSQL dump e.g. from PgAdmin restore tool/Heroku Postgres backup):

1. Create a database in PostgreSQL: `psql -U YOUR_USER_NAME -c "CREATE DATABASE YOUR_DB_NAME;"`
2. Create a schema named `public` and add the following extensions:
    - `pgcrypto`
    - `cube`
    - `earthdistance`
3. Use `pg_restore -U YOUR_USER_NAME -d YOUR_DB_NAME ./YOUR_DATABASE_DUMP`
    - you may get some errors about the role; you can safely ignore those

For local development (with a plain SQL script):

- use `psql [DATABASE_CONNECTION_STRING_HERE] < TABLE_BACKUP.sql`

### VSCode

- Make sure to open this folder via WSL to access Go tools.

<https://code.visualstudio.com/docs/remote/wsl#_advanced-forcing-an-extension-to-run-locally-remotely>

## Go

### Go Modules

When adding new libraries as dependancies, run `go get ...`; this will update `go.mod`.
Run `go mod tidy` to clean up unused libraries in `go.mod`.
