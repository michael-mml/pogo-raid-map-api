package handlers

import (
	"net/http"

	"pogo-raid-map-api/api/models"

	"github.com/labstack/echo/v4"
)

// these endpoints are methods on the Handler struct, so we call them on the Handler instance

// PostTrainerCode inserts a shared trainer code into the database
func (h *Handler) PostTrainerCode(c echo.Context) (err error) {
	sharedTrainerCode := new(models.ShareTrainerCode)
	if err = c.Bind(sharedTrainerCode); err != nil { // bind request body to struct
		return err
	}
	// check if raid id is valid
	rows, err := h.DB.NamedQuery(
		`
			INSERT INTO shared_trainer_code (raid_id, trainer_code)
			VALUES (
				(SELECT id
				FROM shared_raid_boss
				WHERE id=:raid_id
				),
				:trainer_code
			)
			RETURNING id
		`,
		sharedTrainerCode,
	)
	if err != nil {
		return err
	}
	if rows.Next() {
		// scans only the id field, leaves other fields intact
		rows.StructScan(&sharedTrainerCode)
	}
	return c.JSON(http.StatusCreated, sharedTrainerCode)
}

// GetTrainerCodes returns the list of trainer codes for a particular raid
func (h *Handler) GetTrainerCodes(c echo.Context) (err error) {
	raidID := c.QueryParam("raidID")
	sharedTrainerCodes := []*models.ShareTrainerCode{}

	if err = h.DB.Select(
		&sharedTrainerCodes,
		`
			SELECT id, raid_id, trainer_code
			FROM shared_trainer_code
			WHERE raid_id = $1
		`,
		raidID,
	); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, sharedTrainerCodes)
}
