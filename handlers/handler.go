package handlers

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

func init() {
	fmt.Println("Initializing handlers package.")
}

/*
Handler is a wrapper for HandlerFunc to pass the database to the handlers.
Abstracts the type of database driver we're using from the handlers so it can be swapped out.
*/
type Handler struct {
	DB *sqlx.DB
	// Secondary database (connected to separate instance) for geolocation data
	DBGeolocation *sqlx.DB
}
