package handlers

import (
	"log"
	"math/big"
	"net"
	"net/http"
	"os"
	"pogo-raid-map-api/api/models"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
)

// GetGeolocation returns the user's approx geolocation (longitude, latitude) using IP
func (h *Handler) GetGeolocation(c echo.Context) (err error) {
	var IP string

	// get the IP
	if net.ParseIP(c.QueryParam("ip")) != nil { // if IP parameter is valid
		IP = c.QueryParam("ip")
	} else {
		// TODO: Consider smarter geolocation if proxies are used https://husobee.github.io/golang/ip-address/2015/12/17/remote-ip-go.html
		// handles X-REAL-IP, X-FORWARDED-FOR, and fallback remoteAddr
		IP = c.RealIP()
	}
	if IP == "::1" || IP == "127.0.0.1" { // if localhost, use our public IP
		IP = os.Getenv("PUBLIC_IP")
		if IP == "" {
			log.Fatal("Your public IP has not been set for development, please set PUBLIC_IP; exiting the server")
		}
	}

	// convert IP to decimal because ip2location database uses decimal IPs as primary key
	var decimalIP big.Int = *(big.NewInt(0))
	if net.ParseIP(IP).To4() != nil {
		IPSubBlocks := strings.Split(IP, ".")
		IPSubBlock0, err := strconv.ParseUint(IPSubBlocks[0], 10, 64)
		if err != nil {
			return err
		}
		IPSubBlock1, err := strconv.ParseUint(IPSubBlocks[1], 10, 64)
		if err != nil {
			return err
		}
		IPSubBlock2, err := strconv.ParseUint(IPSubBlocks[2], 10, 64)
		if err != nil {
			return err
		}
		IPSubBlock3, err := strconv.ParseUint(IPSubBlocks[3], 10, 64)
		if err != nil {
			return err
		}
		// to account for mapping to ipv6: https://blog.ip2location.com/knowledge-base/ipv4-mapped-ipv6-address/
		decimalIP.SetUint64(IPSubBlock0*16777216 + IPSubBlock1*65536 + IPSubBlock2*256 + IPSubBlock3 + 281470681743360)
	} else if net.ParseIP(IP).To16() != nil {
		IPSubBlocks := strings.Split(IP, ":")
		IPSubBlock0, err := strconv.ParseUint(IPSubBlocks[0], 16, 64)
		if err != nil {
			return err
		}
		IPSubBlock1, err := strconv.ParseUint(IPSubBlocks[1], 16, 64)
		if err != nil {
			return err
		}
		IPSubBlock2, err := strconv.ParseUint(IPSubBlocks[2], 16, 64)
		if err != nil {
			return err
		}
		IPSubBlock3, err := strconv.ParseUint(IPSubBlocks[3], 16, 64)
		if err != nil {
			return err
		}
		IPSubBlock4, err := strconv.ParseUint(IPSubBlocks[4], 16, 64)
		if err != nil {
			return err
		}
		IPSubBlock5, err := strconv.ParseUint(IPSubBlocks[5], 16, 64)
		if err != nil {
			return err
		}
		IPSubBlock6, err := strconv.ParseUint(IPSubBlocks[6], 16, 64)
		if err != nil {
			return err
		}
		IPSubBlock7, err := strconv.ParseUint(IPSubBlocks[7], 16, 64)
		if err != nil {
			return err
		}

		var bases big.Int
		var mult big.Int
		var IPBlock big.Int
		// TODO: refactor below into a loop
		decimalIP.Add(
			&decimalIP,
			mult.Mul(bases.Exp(big.NewInt(65536), big.NewInt(7), nil), IPBlock.SetUint64(IPSubBlock0)),
		)
		decimalIP.Add(
			&decimalIP,
			mult.Mul(bases.Exp(big.NewInt(65536), big.NewInt(6), nil), IPBlock.SetUint64(IPSubBlock1)),
		)
		decimalIP.Add(
			&decimalIP,
			mult.Mul(bases.Exp(big.NewInt(65536), big.NewInt(5), nil), IPBlock.SetUint64(IPSubBlock2)),
		)
		decimalIP.Add(
			&decimalIP,
			mult.Mul(bases.Exp(big.NewInt(65536), big.NewInt(4), nil), IPBlock.SetUint64(IPSubBlock3)),
		)
		decimalIP.Add(
			&decimalIP,
			mult.Mul(bases.Exp(big.NewInt(65536), big.NewInt(3), nil), IPBlock.SetUint64(IPSubBlock4)),
		)
		decimalIP.Add(
			&decimalIP,
			mult.Mul(bases.Exp(big.NewInt(65536), big.NewInt(2), nil), IPBlock.SetUint64(IPSubBlock5)),
		)
		decimalIP.Add(
			&decimalIP,
			mult.Mul(bases.Exp(big.NewInt(65536), big.NewInt(1), nil), IPBlock.SetUint64(IPSubBlock6)),
		)
		decimalIP.Add(
			&decimalIP,
			mult.Mul(bases.Exp(big.NewInt(65536), big.NewInt(0), nil), IPBlock.SetUint64(IPSubBlock7)),
		)
	}

	// query DB to find the latitude and longitude
	location := models.Location{}
	if err := h.DBGeolocation.Get(
		&location,
		`
			SELECT latitude, longitude
			FROM ip2location_db5_v6
			WHERE $1 <= ip_to ORDER BY ip_to LIMIT 1
		`,
		// NUMERIC data type has to be passed as a string
		// https://stackoverflow.com/a/48634020
		decimalIP.String(),
	); err != nil {
		return err
	}

	return c.JSON(http.StatusOK, location)
}
