package handlers

import (
	"net/http"
	"time"

	"pogo-raid-map-api/api/models"

	"github.com/labstack/echo/v4"
)

// ECMAScriptISO8601Format is the format for ECMAScript's toISOString
const ECMAScriptISO8601Format = "2006-01-02T15:04:05.000Z"

// GetRaidBosses returns the current rotation of raid bosses
func (h *Handler) GetRaidBosses(c echo.Context) error {
	raidBosses := []models.RaidBoss{}
	if err := h.DB.Select(
		&raidBosses,
		`
			SELECT id, name, number, tier
			FROM raid_boss
			WHERE name IS NOT NULL
				AND number IS NOT NULL
				AND in_rotation
			ORDER BY tier
			DESC
		`,
	); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, raidBosses)
}

// PostRaidBoss inserts a shared raid boss into the database
func (h *Handler) PostRaidBoss(c echo.Context) (err error) {
	sharedRaidBoss := new(models.ShareRaidBoss)
	if err = c.Bind(sharedRaidBoss); err != nil { // bind request body to struct
		return err
	}
	// check if the share date and time is valid
	if sharedRaidBoss.HatchUTCTimestamp.Before(time.Now().UTC()) {
		return c.String(http.StatusBadRequest, "Invalid share; hatch time and date has already passed.")
	}

	// TODO: see if we can use struct pointer notation
	// (which requires ShareRaidBoss to change the Location field to non-nested)
	// RETURNING: https://github.com/jmoiron/sqlx/issues/83#issuecomment-54839118
	rows, err := h.DB.NamedQuery(
		`
			INSERT INTO shared_raid_boss (boss_id, name, number, tier, hatch_utc_timestamp,
			longitude, latitude, user_status)
			VALUES (
				(SELECT id
				FROM raid_boss
				WHERE name=:name AND tier=:tier AND in_rotation),
				(SELECT name
				FROM raid_boss
				WHERE id=
					(SELECT id
					FROM raid_boss
					WHERE name=:name AND tier=:tier AND in_rotation)
				),
				(SELECT number
				FROM raid_boss
				WHERE id=
					(SELECT id
					FROM raid_boss
					WHERE name=:name AND tier=:tier AND in_rotation)
				),
				(SELECT tier
				FROM raid_boss
				WHERE id=
					(SELECT id
					FROM raid_boss
					WHERE name=:name AND tier=:tier AND in_rotation)
				),
				:hatch_utc_timestamp,
				:longitude,
				:latitude,
				:user_status
			)
			RETURNING id, number
		`,
		map[string]interface{}{
			"name":                sharedRaidBoss.RaidBossName,
			"number":              sharedRaidBoss.RaidBossNumber,
			"tier":                sharedRaidBoss.RaidBossTier,
			"hatch_utc_timestamp": sharedRaidBoss.HatchUTCTimestamp,
			"longitude":           sharedRaidBoss.Location.Longitude,
			"latitude":            sharedRaidBoss.Location.Latitude,
			"user_status":         sharedRaidBoss.UserStatus,
		})
	if err != nil {
		return c.String(http.StatusBadRequest, "Invalid share; either Pokemon and tier do not match or a raid already exists at this time.")
	}
	// TODO: is it faster to scan the id and mutate the sharedRaidBoss object (see below)
	// var id uuid.UUID
	// if rows.Next() {
	// 	rows.Scan(&id)
	// }
	// sharedRaidBoss.RaidID = id
	if rows.Next() {
		// scans only the id and number field (requester does not have id and may supply invalid number), leaves other fields intact
		rows.StructScan(&sharedRaidBoss)
	}
	return c.JSON(http.StatusCreated, sharedRaidBoss)
}

/*
GetNearbyRaidBosses returns a list of available raid bosses that are nearby the given location (as coordinates)
*/
func (h *Handler) GetNearbyRaidBosses(c echo.Context) (err error) {
	longitude := c.QueryParam("longitude")
	latitude := c.QueryParam("latitude")
	maxRadiusInMiles := 1.86411

	sharedRaidBosses := []*models.ShareRaidBoss{} // array of struct pointers s.t. fields can be modified in place
	if err = h.DB.Select(
		&sharedRaidBosses,
		`
			SELECT SRB.id, SRB.name, RB.canonical_name, RB.number, SRB.tier, SRB.hatch_utc_timestamp, SRB.longitude, SRB.latitude, SRB.user_status
			FROM shared_raid_boss as SRB
			INNER JOIN raid_boss as RB
			ON SRB.boss_id = RB.id AND SRB.name = RB.name AND SRB.tier = RB.tier
			WHERE point(longitude,latitude) <@> point($1,$2) < $3
				AND hatch_utc_timestamp >= $4
		`,
		longitude,
		latitude,
		maxRadiusInMiles,
		time.Now().UTC().Format(ECMAScriptISO8601Format), // ISO-8601
	); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, sharedRaidBosses)
}
