package main

import (
	"fmt"
	"net/http"
	"os"

	// "golang.org/x/oauth2"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"pogo-raid-map-api/api/db"
	"pogo-raid-map-api/api/handlers"

	_ "github.com/lib/pq" // alias pq to _
)

func main() {
	e := echo.New()

	// middleware
	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(
		middleware.Logger(),
		middleware.Recover(), // recover from panic() anywhere
		middleware.Secure(),
		middleware.Gzip(),
		middleware.RequestID(), // adds request id to logs
	)
	// TODO: double check if CORS must be enabled
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{
			"http://localhost:8000",
			"http://localhost:8001",
			"http://localhost:8080",
			"http://localhost:8081",
			"https://michael-mml.github.io",
			"https://michael-mml.gitlab.io",
		},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	// override default error handler
	e.HTTPErrorHandler = func(err error, c echo.Context) {
		// format the error and context, save it to a log file/error service perhaps
		fmt.Println(c.Path(), c.QueryParams(), err.Error())

		// call the default handler to return the HTTP response
		e.DefaultHTTPErrorHandler(err, c)
	}

	// connect to db; use this one as a global
	dbx := db.ConnectToDB("postgres", db.GenerateDBXConnection())
	// TODO: refactor secondary database
	dbx2 := db.ConnectToDB("postgres", db.GenerateDBXConnectionGCE())

	authAPI := e.Group("/api") // groups all calls under /auth to ensure user is logged in

	handler := &handlers.Handler{DB: dbx, DBGeolocation: dbx2} // need the Handler instance to call endpoints

	// handler.XXX matches type HandlerFunc: https://godoc.org/github.com/labstack/echo#HandlerFunc
	authAPI.GET("/raid_bosses", handler.GetRaidBosses)
	authAPI.POST("/raid_boss", handler.PostRaidBoss)
	authAPI.GET("/raid_boss/nearby", handler.GetNearbyRaidBosses)
	authAPI.POST("/trainer_codes", handler.PostTrainerCode)
	authAPI.GET("/trainer_codes", handler.GetTrainerCodes)
	authAPI.GET("/geolocation", handler.GetGeolocation)

	// PORT environment variable is set by Heroku web dynos
	port := os.Getenv("PORT")
	// locally, this environment variable may not be defined, so it is explicitly set
	if port == "" {
		os.Setenv("PORT", "1323")
		port = os.Getenv("PORT")
	}
	e.Logger.Fatal(e.Start(":" + port))
}
