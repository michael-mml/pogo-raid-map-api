package models

import (
	"time"

	"github.com/google/uuid"
)

// RaidBoss represents a raid boss' characteristics
type RaidBoss struct {
	ID     uint16 `json:"id" db:"id"`
	Name   string `json:"name" db:"name"`
	Number uint16 `json:"number" db:"number"`
	Tier   string `json:"tier" db:"tier"`
}

// ShareRaidBoss represents the required data to share/report on a raid boss
type ShareRaidBoss struct {
	RaidID uuid.UUID `json:"raidID" db:"id"`
	// TODO: consider replacing the 4 RaidBoss* fields with the RaidBoss struct
	RaidBossID            uint16    `json:"raidBossID,omitempty" db:"boss_id"`
	RaidBossName          string    `json:"raidBossName" db:"name"`
	RaidBossCanonicalName string    `json:"raidBossCanonicalName" db:"canonical_name"`
	RaidBossNumber        uint16    `json:"raidBossNumber" db:"number"`
	RaidBossTier          string    `json:"raidBossTier" db:"tier"`
	HatchUTCTimestamp     time.Time `json:"hatchUTCTimestamp" db:"hatch_utc_timestamp"`
	// TODO: look into whether having a Location struct is better than storing
	// longitude and latitude inside ShareRaidBoss
	Location   `json:"location"`
	UserStatus bool `json:"userStatus" db:"user_status"` // true = "Going", false = "Reporting only"
}

// ShareTrainerCode represents the required data to share a friend code to a shared raid
type ShareTrainerCode struct {
	ShareCodeID uuid.UUID `json:"shareCodeID" db:"id"`
	RaidID      uuid.UUID `json:"raidID" db:"raid_id"`
	// store as a number to enforce type integrity, re-format it on the server
	TrainerCode uint64 `json:"trainerCode" db:"trainer_code"`
}

// Location represents the various ways of defining a gym/Pokestop's location
// currently only LngLat is used, but perhaps postal codes could work too
type Location struct {
	Longitude float32 `json:"longitude" db:"longitude"`
	Latitude  float32 `json:"latitude" db:"latitude"`
}
