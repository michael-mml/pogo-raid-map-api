module pogo-raid-map-api/api

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/echo/v4 v4.1.8
	github.com/lib/pq v1.2.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	google.golang.org/appengine v1.6.1 // indirect
)
