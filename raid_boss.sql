--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Ubuntu 11.4-1.pgdg16.04+1)
-- Dumped by pg_dump version 11.4 (Ubuntu 11.4-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: raid_boss; Type: TABLE; Schema: public; Owner: buoerfvbllgfds
--

CREATE TABLE public.raid_boss (
    id integer NOT NULL,
    number integer NOT NULL,
    name character varying(30) NOT NULL,
    tier integer NOT NULL
);


ALTER TABLE public.raid_boss OWNER TO buoerfvbllgfds;

--
-- Name: raid_boss_id_seq; Type: SEQUENCE; Schema: public; Owner: buoerfvbllgfds
--

CREATE SEQUENCE public.raid_boss_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.raid_boss_id_seq OWNER TO buoerfvbllgfds;

--
-- Name: raid_boss_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: buoerfvbllgfds
--

ALTER SEQUENCE public.raid_boss_id_seq OWNED BY public.raid_boss.id;


--
-- Name: raid_boss id; Type: DEFAULT; Schema: public; Owner: buoerfvbllgfds
--

ALTER TABLE ONLY public.raid_boss ALTER COLUMN id SET DEFAULT nextval('public.raid_boss_id_seq'::regclass);


--
-- Data for Name: raid_boss; Type: TABLE DATA; Schema: public; Owner: buoerfvbllgfds
--

COPY public.raid_boss (id, number, name, tier) FROM stdin;
1	150	armoured mewtwo	5
2	386	deoxys	5
3	248	tyranitar	4
4	105	alola marowak	4
5	359	absol	4
6	275	shiftry	4
7	68	machamp	3
8	26	alola raichu	3
9	94	gengar	3
10	123	scyther	3
11	319	sharpedo	2
12	103	alola exeggutor	2
13	200	misdreavus	2
14	204	pineco	2
15	215	sneasel	2
16	303	mawile	2
17	32	nidoran♂	1
18	116	horsea	1
19	353	shuppet	1
20	355	duskull	1
21	403	shinx	1
22	425	drifloon	1
\.


--
-- Name: raid_boss_id_seq; Type: SEQUENCE SET; Schema: public; Owner: buoerfvbllgfds
--

SELECT pg_catalog.setval('public.raid_boss_id_seq', 22, true);


--
-- Name: raid_boss raid_boss_pkey; Type: CONSTRAINT; Schema: public; Owner: buoerfvbllgfds
--

ALTER TABLE ONLY public.raid_boss
    ADD CONSTRAINT raid_boss_pkey PRIMARY KEY (name);


--
-- Name: raid_boss unique_raid_boss; Type: CONSTRAINT; Schema: public; Owner: buoerfvbllgfds
--

ALTER TABLE ONLY public.raid_boss
    ADD CONSTRAINT unique_raid_boss UNIQUE (name, tier);


--
-- Name: LANGUAGE plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO buoerfvbllgfds;


--
-- PostgreSQL database dump complete
--

