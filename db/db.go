package db

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/jmoiron/sqlx"
)

/*
https://stackoverflow.com/questions/48226789/elegant-way-to-check-if-multiple-strings-are-empty
*/
func containsEmpty(strings ...string) bool {
	for _, str := range strings {
		if str == "" {
			return true
		}
	}
	return false
}

// GenerateDBXConnection returns the connection string
func GenerateDBXConnection() string {
	// get DB connection string from environment vars
	host, portVar, user, password, name := os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_NAME")
	var port int
	port, err := strconv.Atoi(portVar)
	if err != nil || containsEmpty(host, portVar, user, password, name) {
		log.Fatal("DB connection is malformed")
	}

	connString := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=require",
		host, port, user, password, name)
	return connString
}

// GenerateDBXConnectionGCE returns the connection string to GCE-hosted PostgreSQL
// TODO: refactor secondary database
func GenerateDBXConnectionGCE() string {
	// get DB connection string from environment vars
	host, portVar, user, password, name := "34.74.236.174", os.Getenv("DB_PORT"), "postgres", os.Getenv("DB_GCE_PASSWORD"), "postgres"
	var port int
	port, err := strconv.Atoi(portVar)
	if err != nil || containsEmpty(host, portVar, user, password, name) {
		log.Fatal("DB connection is malformed")
	}

	connString := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=require",
		host, port, user, password, name)
	return connString
}

// ConnectToDB connects to the database and returns the database object
func ConnectToDB(connType string, connString string) *sqlx.DB {
	db, err := sqlx.Connect(connType, connString)
	if err != nil {
		// TODO: add more specific error statement to figure out which database failed to connect
		log.Println(err)
		log.Fatal("Error connectToDB 1")
	}
	return db
}
